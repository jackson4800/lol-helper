#include <fstream>
#include <thread>
#include <format>

#include <lol-core.hpp>
#include <curl/curl.h>

#include "easywsclient.hpp"

using namespace std::chrono_literals;

namespace lol {

  std::unique_ptr<client> client::create() {
    return std::make_unique<client>();
  }

  client::client()
    : lcu_(nullptr),
    connected_(false) {
    curl_global_init(CURL_GLOBAL_ALL);
  }

  client::~client() {
    on_lockfile_removed();
    curl_global_cleanup();
  }

  void client::on_lockfile_update(std::filesystem::path lockfile) {

    std::filesystem::copy_file(lockfile.string() + "lockfile", lockfile.string() + "lockfile.bak");

    std::fstream fs(lockfile.string() + "lockfile.bak", std::ios::in | std::ios::binary | std::ios::ate);
    std::string raw_lockfile(fs.tellg(), 0);

    fs.seekg(0, std::ios::beg);
    fs.read(raw_lockfile.data(), raw_lockfile.size());
    fs.close();

    std::filesystem::remove(lockfile.string() + "lockfile.bak");

    std::vector<std::string> lockfile_data;
    split(raw_lockfile, ":", lockfile_data);

    creds_.protocol = lockfile_data[4];
    creds_.password = lockfile_data[3];
    creds_.port = lockfile_data[2];
    creds_.username = "riot";
    creds_.address = "127.0.0.1";

    mtx_.lock();

    if (!connected_) {

      connected_ = true;
      lcu_worker_ = std::thread(worker, this);
      connect(creds_);
    }

    mtx_.unlock();
  }

  void client::on_lockfile_removed() {

    disconnect();

    mtx_.lock();

    connected_ = false;
    lcu_worker_.join();

    if (lcu_) {

      delete lcu_;
      lcu_ = nullptr;
    }

    mtx_.unlock();
  }

  const char* client::get_name_by_id(int id) {
    return atlas_[id];
  }

  int client::get_id_by_name(const char* name) {
    return atlas_[name];
  }

  bool client::init_lcu() {

    if (lcu_) {

      delete lcu_;
      lcu_ = nullptr;
    }

    lcu_ = lol::WebSocket::from_url(
      std::format("wss://{}:{}/", creds_.address, creds_.port),
      std::format("{}:{}", creds_.username, creds_.password),
      ""
    );

    if (!lcu_ || lcu_->getReadyState() == WebSocket::CLOSED)
      return false;

    lcu_->send("[5, \"OnJsonApiEvent\"]");

    return true;
  }

  void client::worker(client* this_) {

    while (this_->connected_) {

      if ((!this_->lcu_ || this_->lcu_->getReadyState() == WebSocket::CLOSED)
        && !this_->init_lcu()) {

        std::this_thread::sleep_for(5s);
        continue;
      }

      this_->lcu_->poll();
      this_->lcu_->dispatch([this_](const auto& msg) {
        try {
          auto js = lol::json::parse(msg, nullptr, false);

          if (js.is_discarded())
            return;

          if (!js.is_array())
            return;

          if (js[1] != "OnJsonApiEvent")
            return;

          const auto& message = js[2];

          const auto endpoint = std::format("{}:{}", message["uri"].get<std::string>(),
            message["eventType"].get<std::string>());

          this_->lcu_message(this_, endpoint, message);

          auto it = this_->endpoint_events.find(endpoint);

          if (it != this_->endpoint_events.end())
            it->second(this_, endpoint, message);
        }
        catch (const std::exception& ex) {
          return;
        }
      });
    }
  }

}