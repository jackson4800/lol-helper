#include <regex>
#include <cctype>
#include <lol-core.hpp>


namespace lol {

  ddragon_atlas::ddragon_atlas() {

    auto api_request = request::get("https://ddragon.leagueoflegends.com/api/versions.json");

    if (200 != api_request->perform())
      return;

    auto api_buffer = api_request->get_data_buffer();
    version_ = json::parse(api_buffer->data())[0];

    auto champions = request::get(std::format("https://ddragon.leagueoflegends.com/cdn/{}/data/en_US/champion.json", version_));
    if (200 != champions->perform())
      return;

    auto buffer = champions->get_data_buffer();
    auto champ_data = json::parse(buffer->data(), nullptr, false);

    if (champ_data.is_discarded())
      return;

    for (auto& v : champ_data["data"]) {

      auto id = std::atoi(v["key"].get<std::string>().c_str());
      auto name = v["name"].get<std::string>();
      std::transform(name.begin(), name.end(), name.begin(), [](auto c) -> char { return std::tolower(c); });

      id_to_names_[id] = name;
      name_to_ids_[name] = id;
    }
  }

  ddragon_atlas::~ddragon_atlas() {  }

  const char* ddragon_atlas::operator[](int id) const {
    auto it = id_to_names_.find(id);

    if (it == id_to_names_.end())
      return nullptr;

    return it->second.c_str();
  }

  int ddragon_atlas::operator[](std::string_view name) const {
    auto it = name_to_ids_.find(name.data());

    if (it == name_to_ids_.end())
      return -1;

    return it->second;
  }
}