#include <Windows.h>
#include <TlHelp32.h>

#include <thread>
#include <chrono>
#include <cwctype>
#include <filesystem>

#include <lol-core.hpp>

using namespace std::chrono_literals;

namespace lol {

  void split(std::string_view in, std::string_view split, std::vector<std::string>& out) {

    auto pos = -1;
    do {
      auto old_pos = pos + 1;
      pos = in.find(split, old_pos);

      out.emplace_back(in.substr(old_pos, pos - old_pos));
    } while (pos != std::string::npos);
  }

  std::filesystem::path wait_for_process(std::string_view name) {

    std::filesystem::path result;
    std::wstring wide_name(name.begin(), name.end());
    std::transform(wide_name.begin(), wide_name.end(),
      wide_name.begin(), std::towlower);

    do {

      auto snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

      if (snapshot == INVALID_HANDLE_VALUE)
        continue;

      PROCESSENTRY32 pe;
      pe.dwSize = sizeof(pe);

      if (!Process32First(snapshot, &pe)) {

        CloseHandle(snapshot);
        continue;
      }

      do {
        auto process = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pe.th32ProcessID);

        if (process == INVALID_HANDLE_VALUE)
          continue;

        DWORD filepath_size = MAX_PATH;
        TCHAR filepath[MAX_PATH];

        if (QueryFullProcessImageName(process, 0, filepath, &filepath_size)) {


          std::wstring str_filepath(filepath, filepath + filepath_size);
          auto old = str_filepath;

          std::transform(str_filepath.begin(), str_filepath.end(),
            str_filepath.begin(), std::towlower);

          if (str_filepath.find(wide_name) != std::string::npos)
            result = old;
        }

        CloseHandle(process);
      } while (Process32Next(snapshot, &pe) && result.empty());

      CloseHandle(snapshot);

      std::this_thread::sleep_for(5ms);
    } while (result.empty());

    return result;
  }
}