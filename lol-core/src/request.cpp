#include <lol-core.hpp>
#include <curl/curl.h>

#include <format>

#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>

namespace lol {

  request::buffer::buffer() noexcept { clear(); }
  request::buffer::~buffer() noexcept {
    clear();
  }

  request::buffer::buffer(buffer&& other) noexcept {
    clear();
    data_.insert(data_.begin(), other.data_.begin(), other.data_.end());
  }

  request::request(std::string_view url, const int method)
    : url_(url), method_(method),
    data_buffer_(std::make_shared<buffer>()),
    curl_(curl_easy_init()),
    request_headers_(nullptr),
    cookies_(nullptr) {
    add_header("Accept: application/json");
    add_header("Content-Type: application/json; charset: utf-8");
  }

  request::~request() {


    if (request_headers_)
      curl_slist_free_all(request_headers_);

    if (curl_)
      curl_easy_cleanup(curl_);
  }

  int request::repeat() {

    error_buffer[0] = '\0';

    if (curl_) {

      auto code = curl_easy_perform(curl_);

      if (code == CURLE_OK) {

        int response_code;
        curl_easy_getinfo(curl_, CURLINFO_RESPONSE_CODE, &response_code);

        return response_code;
      }

      return code | (1 << 31);
    }

    return std::numeric_limits<int>::min();
  }

  int request::perform() {

    error_buffer[0] = '\0';

    if (curl_) {

      if (method_ == method::HTTP_POST || method_ == method::HTTP_PUT) {

        auto raw_json = this->request_form_.dump();
        data_buffer_->write(
          reinterpret_cast<std::uint8_t*>(raw_json.data()), raw_json.size());

        if (method_ == method::HTTP_POST) {
          curl_easy_setopt(curl_, CURLOPT_POST, 1L);
          curl_easy_setopt(curl_, CURLOPT_POSTFIELDSIZE, raw_json.size());
        };

        curl_easy_setopt(curl_, CURLOPT_READFUNCTION, read_callback);
        curl_easy_setopt(curl_, CURLOPT_READDATA, data_buffer_.get());
      }

      if (!headers_.empty()) {

        request_headers_ = nullptr;

        for (auto& header : headers_)
          request_headers_ = curl_slist_append(request_headers_, header.c_str());

        curl_easy_setopt(curl_, CURLOPT_HTTPHEADER, request_headers_);
      }

      curl_easy_setopt(curl_, CURLOPT_COOKIEFILE, "");

      if (cookies_) {
        struct curl_slist* each = cookies_;

        while (each) {
          curl_easy_setopt(curl_, CURLOPT_COOKIELIST, each->data);

          each = each->next;
        }
      }

      if (method_ == method::HTTP_PUT)
        curl_easy_setopt(curl_, CURLOPT_PUT, 1);

      if (method_ == method::HTTP_HEAD)
        curl_easy_setopt(curl_, CURLOPT_CUSTOMREQUEST, "HEAD");
      else if (method_ == method::HTTP_DELETE)
        curl_easy_setopt(curl_, CURLOPT_CUSTOMREQUEST, "DELETE");
      else if (method_ == method::HTTP_PATCH)
        curl_easy_setopt(curl_, CURLOPT_CUSTOMREQUEST, "PATCH");
      else if (method_ == method::HTTP_OPTIONS)
        curl_easy_setopt(curl_, CURLOPT_CUSTOMREQUEST, "OPTIONS");

      curl_easy_setopt(curl_, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS | CURLPROTO_HTTP);

      curl_easy_setopt(curl_, CURLOPT_WRITEDATA, data_buffer_.get());
      curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, write_callback);

      curl_easy_setopt(curl_, CURLOPT_FOLLOWLOCATION, 1);

      curl_easy_setopt(curl_, CURLOPT_URL, url_.c_str());

      curl_easy_setopt(curl_, CURLOPT_ERRORBUFFER, error_buffer);
      curl_easy_setopt(curl_, CURLOPT_USERAGENT, "lol/Helper");

      curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYHOST, FALSE);

      auto code = curl_easy_perform(curl_);

      if (code == CURLE_OK) {

        int response_code;
        curl_easy_getinfo(curl_, CURLINFO_RESPONSE_CODE, &response_code);

        return response_code;
      }

      return code | (1 << 31);
    }

    return std::numeric_limits<int>::min();
  }

  void request::add_auth(std::string_view user, std::string_view pass) {
    using namespace boost::archive::iterators;
    using it = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;

    auto raw_auth = std::format("{}:{}", user, pass);
    auto auth = std::string(it(raw_auth.begin()), it(raw_auth.end()));

    std::replace(auth.begin(), auth.end(), '+', '-');
    std::replace(auth.begin(), auth.end(), '/', '_');

    add_header(std::format("Authorization: Basic {}", auth));
  }

  curl_slist* request::get_cookies() {
    struct curl_slist* cookies = NULL;
    curl_easy_getinfo(curl_, CURLINFO_COOKIELIST, &cookies);

    return cookies;
  }

  const char* request::get_error_description(int result) {
    if (!(result & (1 << 31)))
      return nullptr;

    result &= ~(1 << 31);

    if (error_buffer[0] != 0)
      return error_buffer;

    if (result < CURL_LAST)
      return curl_easy_strerror(static_cast<CURLcode>(result));

    return nullptr;
  }

  std::shared_ptr<request::buffer> request::get_data_buffer() { return data_buffer_; }
  void request::set_form(const json& data) { request_form_ = data; }
  void request::add_header(std::string_view header) { headers_.push_back(std::string(header)); }
  void request::add_form_part(std::string_view name, const json& data) { request_form_[name.data()] = data; }
  void request::add_cookies(curl_slist* cookies) { cookies_ = cookies; }

  std::shared_ptr<request> request::get(std::string_view url) { return std::make_shared<request>(url, method::HTTP_GET); }
  std::shared_ptr<request> request::post(std::string_view url) { return std::make_shared<request>(url, method::HTTP_POST); }
  std::shared_ptr<request> request::put(std::string_view url) { return std::make_shared<request>(url, method::HTTP_PUT); }
  std::shared_ptr<request> request::head(std::string_view url) { return std::make_shared<request>(url, method::HTTP_HEAD); }
  std::shared_ptr<request> request::del(std::string_view url) { return std::make_shared<request>(url, method::HTTP_DELETE); }
  std::shared_ptr<request> request::patch(std::string_view url) { return std::make_shared<request>(url, method::HTTP_PATCH); }
  std::shared_ptr<request> request::options(std::string_view url) { return std::make_shared<request>(url, method::HTTP_OPTIONS); }

  std::size_t request::write_callback(std::uint8_t* buf, std::size_t size, std::size_t nmemb, buffer* data) {

    data->write(buf, size * nmemb);
    return size * nmemb;
  }

  std::size_t request::read_callback(std::uint8_t* buf, std::size_t size, std::size_t nmemb, buffer* data) {

    if (!data->size())
      return 0;

    auto buf_size = size * nmemb;
    auto to_write = data->size() - data->read_pos();
    auto written = std::min(buf_size, to_write);

    std::memcpy(buf, data->raw_data() + data->read_pos(), written);

    data->seek(written);

    if (data->read_pos() == data->size())
      data->clear();

    return written;
  }
}