#pragma once
#include <mutex>
#include <thread>
#include <string>

#include <filesystem>

#include "json.hpp"
#include "dispatcher.hpp"

#ifdef LOL_EXPORTS
#define LOL_API __declspec(dllexport)
#else
#define LOL_API __declspec(dllimport)
#endif

typedef void CURL;
struct curl_slist;

namespace lol {
  class WebSocket;

  class LOL_API ddragon_atlas {
  public:
    ddragon_atlas();
    ~ddragon_atlas();
  public:
    const char* operator[](int id) const;
    int operator[](std::string_view name) const;
  protected:
    std::string version_;
    std::unordered_map<int, std::string> id_to_names_;
    std::unordered_map<std::string, int> name_to_ids_;
  };

  class LOL_API client {
  public:
    static std::unique_ptr<client> create();
  public:
    struct LOL_API credentials {
      std::string protocol;
      std::string address;
      std::string port;
      std::string username;
      std::string password;
    };
  public:
    client();
    ~client();
  public:
    void on_lockfile_update(std::filesystem::path lockfile);
    void on_lockfile_removed();
  public:
    const char* get_name_by_id(int id);
    int get_id_by_name(const char* name);
  protected:
    bool init_lcu();
  protected:
    static void worker(client* _this);
  protected:
    credentials creds_;
  public:
    using endpoint_event = dispatcher<client*, const std::string&, const json&>;
    dispatcher<const credentials&> connect;
    dispatcher<client*, std::string, const json&> lcu_message;
    dispatcher<> disconnect;
    std::unordered_map<std::string, endpoint_event> endpoint_events;
  protected:
    std::thread lcu_worker_;
    bool connected_;
    WebSocket* lcu_;
    std::mutex mtx_;
  protected:
    ddragon_atlas atlas_;
  };

  class LOL_API request {
  protected:
    struct method {
      static constexpr auto HTTP_GET = 0;
      static constexpr auto HTTP_POST = 1;
      static constexpr auto HTTP_PUT = 2;
      static constexpr auto HTTP_HEAD = 3;
      static constexpr auto HTTP_DELETE = 4;
      static constexpr auto HTTP_PATCH = 5;
      static constexpr auto HTTP_OPTIONS = 6;
    };
  public:
    class buffer {
    protected:
      std::size_t read_pos_;
      std::vector<std::uint8_t> data_;
    public:
      buffer() noexcept;
      ~buffer() noexcept;
    public:
      buffer(buffer&& other) noexcept;
    public:
      inline void write(std::uint8_t* buf, std::size_t len) { data_.insert(data_.end(), buf, buf + len); }
    public:
      inline void clear() { read_pos_ = 0; data_.clear(); }
      inline std::size_t size() { return data_.size(); }
      inline std::uint8_t* raw_data() { return &data_[0]; }
      inline std::string data() { return std::string(data_.begin(), data_.end()); }
      inline std::size_t read_pos() { return read_pos_; }
      inline void seek(std::size_t off) { read_pos_ += off; }
    };
  protected:
    int method_;
    std::string url_;
    CURL* curl_;
  protected:
    std::shared_ptr<buffer> data_buffer_;
  protected:
    json request_form_;
  protected:
    curl_slist* request_headers_;
    std::list<std::string> headers_;
  protected:
    char error_buffer[256];
  protected:
    curl_slist* cookies_;
  public:
    request(std::string_view url, const int method);
    ~request();
  public:
    const char* get_error_description(int result);
  public:
    int repeat();
    int perform();
    void add_auth(std::string_view user, std::string_view pass);
  public:
    curl_slist* get_cookies();
  public:
    std::shared_ptr<buffer> get_data_buffer();
  public:
    void set_form(const json& data);
    void add_header(std::string_view header);
    void add_form_part(std::string_view name, const json& data);
    void add_cookies(curl_slist* cookies);
  public:
    static std::shared_ptr<request> get(std::string_view url);
    static std::shared_ptr<request> post(std::string_view url);
    static std::shared_ptr<request> put(std::string_view url);
    static std::shared_ptr<request> head(std::string_view url);
    static std::shared_ptr<request> del(std::string_view url);
    static std::shared_ptr<request> patch(std::string_view url);
    static std::shared_ptr<request> options(std::string_view url);
  protected:
    static std::size_t write_callback(std::uint8_t* buf, std::size_t size, std::size_t nmemb, buffer* data);
    static std::size_t read_callback(std::uint8_t* buf, std::size_t size, std::size_t nmemb, buffer* data);
  };

  void LOL_API split(std::string_view in, std::string_view split, std::vector<std::string>& out);
  std::filesystem::path LOL_API wait_for_process(std::string_view name);

  typedef lol::observer<lol::client*, const std::string&, const lol::json&> endpoint_listener_t;
}

#define PLUGIN_API __declspec(dllexport) __cdecl
#define PLUGIN_ENTRY(var) void PLUGIN_API init(lol::client* var)