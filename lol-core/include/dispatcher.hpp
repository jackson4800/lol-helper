#pragma once
#include <list>

#include "observer.hpp"

namespace lol {

  template<class ...Args>
  class dispatcher {
    using node_type = std::shared_ptr<observer<Args...>>;
  public:
    // I wanted to achieve smth that uwp or C# does,
    // so here it is, dk what should i add here
    inline auto& operator+=(node_type obs);
    inline auto& operator+=(observer<Args...>* obs);
    inline auto& operator+=(std::function<void(const Args...)> obs);
    inline void operator()(const Args... args) const;
  protected:
    std::list<node_type> observers_;
  };
}

#include "dispatcher.ipp"