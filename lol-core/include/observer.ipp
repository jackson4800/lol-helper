#pragma once

namespace lol {

  template<class... Args>
  inline observer<Args...>::observer(std::function<void(const Args...)> fn)
    : callback_(fn) {	}

  template<class... Args>
  inline void observer<Args...>::notify(Args... args) {
    callback_(std::forward<Args>(args)...);
  }
}