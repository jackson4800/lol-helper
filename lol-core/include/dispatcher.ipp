#pragma once

namespace lol {

  template<class... Args>
  inline auto& dispatcher<Args...>::operator+=(node_type obs) {
    observers_.push_back(std::move(obs));

    return *this;
  }

  template<class... Args>
  inline auto& dispatcher<Args...>::operator+=(observer<Args...>* obs) {
    observers_.emplace_back(std::move(node_type(obs)));

    return *this;
  }

  template<class... Args>
  inline auto& dispatcher<Args...>::operator+=(std::function<void(const Args...)> fn) {
    observers_.emplace_back(std::move(std::make_shared<observer<Args...>>(fn)));

    return *this;
  }

  template<class... Args>
  inline void dispatcher<Args...>::operator()(const Args... args) const {

    for (auto obs : observers_)
      obs->notify(std::forward<const Args>(args)...);
  }
}