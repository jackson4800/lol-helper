#pragma once
#include <functional>

namespace lol {

  template<class ...Args>
  class observer {
  public:
    inline observer(std::function<void(const Args...)> fn);
  public:
    inline void notify(const Args... args);
  protected:
    std::function<void(const Args...)> callback_;
  };
}

#include "observer.ipp"