#include <iostream>
#include <Windows.h>
#include <lol-core.hpp>

PLUGIN_ENTRY(client) {

  std::cout << "Debug initialized" << std::endl;

  client->lcu_message +=
    [](auto client, const auto& endpoint, const auto& msg) {

    if (endpoint.starts_with("/lol-perks"))
      std::cout << endpoint << std::endl;
  };

  client->endpoint_events["/lol-perks/v1/currentpage:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    std::cout << msg << std::endl;
  };

  client->endpoint_events["/lol-summoner/v1/current-summoner:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    std::cout << msg << std::endl;
  };
  client->endpoint_events["/lol-perks/v1/perks:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    std::cout << msg << std::endl;
  };

}