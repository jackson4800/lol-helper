#include <iostream>
#include <fstream>

#include <Windows.h>
#include <lol-core.hpp>

#define REQUEST(var, fn, route)						\
auto var = lol::request::fn(base_url + route);	\
var->add_auth(username, password)

#define GET(var, route) REQUEST(var, get, route)
#define PUT(var, route) REQUEST(var, put, route)
#define POST(var, route) REQUEST(var, post, route)

lol::json config;

std::string base_url;
std::string username, password;

void load_config() {

  config["page_count"] = 0;
  config["has_free_pages"] = false;
  config["last_installed_id"] = -1;
  config["dummy"] = -1;
  config["in_game"] = false;

  std::fstream fs("runes.json", std::ios::binary | std::ios::in | std::ios::ate);

  if (!fs.is_open())
    return;

  std::string raw_config(fs.tellg(), 0);
  fs.seekg(0, std::ios::beg);
  fs.read(raw_config.data(), raw_config.size());

  config = lol::json::parse(raw_config, nullptr, false);

  if (config.is_discarded())
    config = lol::json{};

  config["last_installed_id"] = -1;
  config["in_game"] = false;
}

void check_for_free_page(const lol::json& pages, lol::client* client) {

  bool has_dummy_page = false;
  auto used = std::count_if(pages.begin(), pages.end(), 
  [&has_dummy_page, client](const auto& page) {

    auto page_name = page["name"].get<std::string>();
    std::transform(page_name.begin(), page_name.end(), page_name.begin(), tolower);

    if (page_name.starts_with("rh_")) {
      config["dummy"] = page["id"];
      has_dummy_page = true;
    }

    if (auto id = client->get_id_by_name(page_name.data()); id != -1) {

      config[std::to_string(id)] = page;

      auto& rune_page = config[std::to_string(id)];
      rune_page["name"] = std::string("RH_") + rune_page["name"].get<std::string>();
      rune_page["current"] = true;
    }

    return page["isEditable"];
  });

  std::fstream fs("runes.json", std::ios::binary | std::ios::out);
  fs << config;

  if (!has_dummy_page)
    config["dummy"] = -1;

  config["has_free_pages"] = used < config["page_count"];
}

void refresh_pages(lol::client* client) {
  GET(req, "/lol-perks/v1/pages/");
  req->perform();

  auto buffer = req->get_data_buffer();

  check_for_free_page(lol::json::parse(buffer->data()), client);
}

void remove_dummy_page() {

  auto dummy_id = config["dummy"];

  if (dummy_id == -1)
    return;

  auto req = lol::request::del(base_url + std::format("/lol-perks/v1/pages/{}", dummy_id.get<std::size_t>()));
  req->add_auth(username, password);

  req->perform();
}

void create_new_page(int champion_id) {

  auto req = lol::request::post(base_url + "/lol-perks/v1/pages/");
  req->add_auth(username, password);
  req->set_form(config[std::to_string(champion_id)]);

  req->perform();

  config["last_installed_id"] = champion_id;
}

void register_endpoint_events(lol::client* client) {

  client->endpoint_events["/lol-perks/v1/pages:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    check_for_free_page(msg["data"], client);
  };

  client->endpoint_events["/lol-perks/v1/inventory:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    refresh_pages(client);
  };

  client->endpoint_events["/lol-perks/v1/currentpage:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    auto page_name = msg["data"]["name"].get<std::string>();
    std::transform(page_name.begin(), page_name.end(), page_name.begin(), tolower);

    if (page_name.starts_with("rh_")) {

      config["dummy"] = msg["data"]["id"];
      return;
    }

    if (auto id = client->get_id_by_name(page_name.data()); id != -1) {

      config[std::to_string(id)] = msg["data"];

      auto& rune_page = config[std::to_string(id)];
      rune_page["name"] = std::string("RH_") + rune_page["name"].get<std::string>();
      rune_page["current"] = true;

      std::fstream fs("runes.json", std::ios::binary | std::ios::out);
      fs << config;
    }
  };

  client->endpoint_events["/lol-champ-select/v1/session:Update"] +=
    [](auto client, const auto& endpoint, const auto& msg) {
    config["in_game"] = true;
    config["local_player"] = msg["data"]["localPlayerCellId"];

    const auto& my_team = msg["data"]["myTeam"];
    auto it = std::find_if(my_team.begin(), my_team.end(), [](const auto& v) {
      return v["cellId"] == config["local_player"];
      });

    if (it == my_team.end())
      return;

    const auto& local_player = *it;

    if (local_player["championId"] == 0)
      return;

    if (config["last_installed_id"] == local_player["championId"])
      return;

    if (!config.contains(std::to_string(local_player["championId"].get<int>())))
      return;

    remove_dummy_page();
    create_new_page(local_player["championId"]);
  };

  client->endpoint_events["/lol-champ-select/v1/session:Delete"] +=
    [](auto client, const auto& endpoint, const auto& msg) {

    config["last_installed_id"] = -1;
    config["in_game"] = false;
  };
}

void register_basic_events(lol::client* client) {

  client->connect +=
    [client](const auto& creds) {

    username = creds.username;
    password = creds.password;
    base_url = std::format("{}://{}:{}", creds.protocol, creds.address, creds.port);

    GET(req, "/lol-perks/v1/inventory");
    req->perform();

    auto buffer = req->get_data_buffer();
    config["page_count"] = lol::json::parse(buffer->data())["ownedPageCount"];

    refresh_pages(client);
  };

  client->disconnect +=
    []() {

    std::fstream fs("runes.json", std::ios::binary | std::ios::out);
    fs << config;
  };
}

PLUGIN_ENTRY(client) {

  load_config();
  register_basic_events(client);
  register_endpoint_events(client);

  std::cout << "Rune helper initialized" << std::endl;
}