#include <WinSock2.h>

#include <memory>
#include <iostream>

#include "filewatch.hpp"
#include <lol-core.hpp>

#define PLUGIN_FOLDER "./plugins/"

std::unique_ptr<lol::client> client = lol::client::create();

void init_plugins() {
  for (const auto& plugin : std::filesystem::directory_iterator(
    std::filesystem::absolute(PLUGIN_FOLDER))
    ) {

    if (plugin.path().extension() != ".dll")
      continue;

    auto plugin_handle = LoadLibraryA(plugin.path().string().c_str());
    const auto init = reinterpret_cast<void(__cdecl*)(lol::client * client)>(
      GetProcAddress(plugin_handle, "?init@@YAXPAVclient@lol@@@Z")
      );

    if (init)
      init(client.get());
  }
}

int main(int argc, char** argv) {
  init_plugins();

  auto path = lol::wait_for_process("LeagueClient.exe");
  path.remove_filename();

  if (std::filesystem::exists(path.string() + "lockfile"))
    client->on_lockfile_update(path);

  filewatch::FileWatch<std::filesystem::path> lockfile_watch(
    path,
    [&path](const auto& fn, const auto evt) {

      if (fn != "lockfile")
        return;

      switch (evt) {

      case filewatch::Event::removed:
        // Disconnect
        break;
      case filewatch::Event::added:
      case filewatch::Event::modified:
        // Notify client about lcu creds update
        client->on_lockfile_update(path);
        break;
      default:
        break;
      }
    }
  );

  while (true)
    std::this_thread::sleep_for(std::chrono::seconds(1));

  return 0;
}